/*
 *  (1) Download and install ZigSim on your phone (iPhone & Android)
 *  (2) Make sure your phone and your computer are on the same network
 *  (3) Run this sketch and note down your IP address
 *  (4) In ZigSim go to settings:
 *    Data Destination: Other App
 *    Protocol:         UDP
 *    IP Address:       fill in the IP address of your computer  
 *    Port Number:      8000
 *    Message Format:   OSC
 *    Message Rate:     10
 *    Device UUID:      1
 *    Compass Angle:    Portrait
 *  (5) In ZigSim go to sensor and select Gravity
 *  (6) In ZigSim press start
 *    You should see something like this here
 *      IP=172.20.10.5, incoming message: addrpattern=/ZIGSIM/1/gravity typetag=fff
 *      IP=172.20.10.5, incoming message: addrpattern=/ZIGSIM/1/deviceinfo typetag=ssssii
 */

import oscP5.*;
import java.net.InetAddress;

InetAddress inet;
String myIP;

OscP5 oscP5;
float x, y, z;

void setup() {
  size(520, 400, P3D);
  
  // get computer's IP adress
  try {
    inet = InetAddress.getLocalHost();
    myIP = inet.getHostAddress();
  }
  catch (Exception e) {
    e.printStackTrace();
    myIP = "couldn't get IP";
  }
  println("IP address of this computer: "+myIP);

  // start oscP5 to listen for incoming messages on port 8000
  oscP5 = new OscP5(this, 8000);
}





/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage incomingMessage) {

  //screenPrintLines.clear();
  // debug messages:
  print("computerIP="+myIP+", ");
  print("incoming message: addrpattern="+incomingMessage.addrPattern());
  println(", typetag="+incomingMessage.typetag());

  // ZIGSIM:

  // show device info
  if (incomingMessage.checkAddrPattern("/ZIGSIM/1/deviceinfo")) {
    String s0 = incomingMessage.get(0).stringValue();
    String s1 = incomingMessage.get(1).stringValue();
    String s2 = incomingMessage.get(2).stringValue();
    String s3 = incomingMessage.get(3).stringValue();
    int i4 = incomingMessage.get(4).intValue();
    int i5 = incomingMessage.get(5).intValue();
    //println(s0,s1,s2,s3,i4,i5);
  }

  if (incomingMessage.checkAddrPattern("/ZIGSIM/1/gravity")) {
    x = incomingMessage.get(0).floatValue();
    y = incomingMessage.get(1).floatValue();
    z = incomingMessage.get(2).floatValue();
    println(x, y, z);
  }
}




void draw() {
  background(255);
  noLights();
  // draw the gravity vector as data bars

  noStroke();
  rectMode(CORNER);

  float barX = map(x, -2, 2, height, -height);
  fill(0xffffaaaa);
  rect(10, height/2, 90, barX);
  fill(0);
  text(x, 20, height/2+20);

  float barY = map(y, -2, 2, height, -height);
  fill(0xffaaffaa);
  rect(110, height/2, 90, barY);
  fill(0);
  text(x, 120, height/2+20);

  float barZ = map(z, -2, 2, height, -height);
  fill(0xffaaaaff);
  rect(210, height/2, 90, barZ);
  fill(0);
  text(x, 220, height/2+20);
  
  //color the cube
  lights();
  translate(400,height/2,0);
  rotateX(QUARTER_PI);
  rotateY(QUARTER_PI);
  fill(map(x,-1,1,0,255),map(y,-1,1,0,255),map(z,-1,1,0,255));
  box(100,100,100);
}
