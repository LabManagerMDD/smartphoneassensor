**Summary

This repo contains a few boilerplate examples for sending gravity data from a phone using the app ZIG SIM to a computer using processing, Unity, Max/MSP or TouchDesigner.

**ZIG SIM Setup

1. Download ZIG SIM: https://1-10.github.io/zigsim/getting-started.html
2. Settings in ZIG SIM:

* DATA DESTINATION: OTHER APP
* PROTOCOL: UDP
* IP ADDRESS: your IP address
* PORT NUMBER: 8000
* MESSAGE FORMAT: OSC
* MESSAGE RATE(PER SEC): 30
* DEVICE UUID: 1
* COMPASS ANGLE: PORTRAIT

