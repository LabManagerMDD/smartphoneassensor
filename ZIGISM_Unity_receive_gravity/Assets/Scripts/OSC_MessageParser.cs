﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSC_MessageParser : MonoBehaviour
{

    [Header("Gravity values being received:")]

    public float gravityX;
    public float gravityY;
    public float gravityZ;

    [Header("Microphone values being received:")]
    public float micLevelMax;
    public float micLevelAverage;

    private OSC osc;

    private void Start()
    {
        osc = GetComponent<OSC>();

        // link incoming gravity message:
        osc.SetAddressHandler("/ZIGSIM/1/gravity", OnGravityMessage);
        osc.SetAddressHandler("/ZIGSIM/1/miclevel", OnMicrophoneMessage);
    }


    private void OnGravityMessage(OscMessage oscM)
    {
        gravityX = oscM.GetFloat(0);
        gravityY = oscM.GetFloat(1);
        gravityZ = oscM.GetFloat(2);
    }

    private void OnMicrophoneMessage(OscMessage oscM)
    {
        micLevelMax = oscM.GetFloat(0);
        micLevelAverage = oscM.GetFloat(1);
    }

}
