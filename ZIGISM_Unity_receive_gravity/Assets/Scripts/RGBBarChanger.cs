﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RGBBarChanger : MonoBehaviour
{

    public OSC_MessageParser oscMessageParser;

    public Transform gravityXCube;
    public Transform gravityYCube;
    public Transform gravityZCube;

    public Text gravityXText;
    public Text gravityYText;
    public Text gravityZText;

    // Update is called once per frame
    void Update()
    {
        gravityXCube.localScale = new Vector3(1, oscMessageParser.gravityX, 1);
        gravityXText.text = oscMessageParser.gravityX.ToString();

        gravityYCube.localScale = new Vector3(1, oscMessageParser.gravityY, 1);
        gravityYText.text = oscMessageParser.gravityY.ToString();

        gravityZCube.localScale = new Vector3(1, oscMessageParser.gravityZ, 1);
        gravityZText.text = oscMessageParser.gravityZ.ToString();
    }
}
