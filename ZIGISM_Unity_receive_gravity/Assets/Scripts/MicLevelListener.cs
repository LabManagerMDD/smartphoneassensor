﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicLevelListener : MonoBehaviour
{

    public float micLevelNoiseFloor = -32;
    public float amplitudeGain = 10;

    private OSC_MessageParser oscMessageParser;
    private Rigidbody _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        oscMessageParser = FindObjectOfType<OSC_MessageParser>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
        float micLevelDb = oscMessageParser.micLevelAverage;

        if (micLevelDb > micLevelNoiseFloor)
        {
            float micLevelAmp = Mathf.Pow(10, micLevelDb / 20);// 10 ^ (A / 20)
            //Debug.LogFormat("{0} db is {1}", micLevelDb, micLevelAmp);
            Vector3 upForce = new Vector3(0, micLevelAmp* amplitudeGain, 0);
            _rigidbody.AddForce(upForce, ForceMode.Acceleration);
        }
    }
}
