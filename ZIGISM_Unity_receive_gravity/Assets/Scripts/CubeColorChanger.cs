﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeColorChanger : MonoBehaviour
{

    public OSC_MessageParser oscMessageParser;

    Material cubeMaterial;

    // Start is called before the first frame update
    void Start()
    {
        cubeMaterial = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        float red = (oscMessageParser.gravityX + 1) / 2; // map incoming gravityX values (-1..1) to color values (0..1)
        float green = (oscMessageParser.gravityY + 1) / 2; // map incoming gravityX values (-1..1) to color values (0..1)
        float blue = (oscMessageParser.gravityZ + 1) / 2; // map incoming gravityX values (-1..1) to color values (0..1)
        cubeMaterial.color = new Color(red, green, blue);
    }
}
