﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class IP_Finder : MonoBehaviour
{

    [Tooltip("Here you will see your IP address when you start the Unity project")]
    public string myIP;

    public string GetMyIP()
    {
        return GetLocalIPAddress();
    }

    private void Start()
    {
        myIP = GetLocalIPAddress();
    }

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }

}
